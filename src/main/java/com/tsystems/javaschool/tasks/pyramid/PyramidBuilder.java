package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;
import java.util.stream.IntStream;

public class PyramidBuilder {
    public static int findM(int N) {
        return -1 + IntStream.range(0, N).map(i -> 2).sum();
    }

    public static int findN(int sizeOfArray) {
        double discriminant = Math.sqrt(1 + 8 * sizeOfArray);
        if (discriminant > 0 && discriminant % 1 == 0) {
            int N = (int) Math.floor((- 1 + discriminant) / 2);
            return N >= 1 ? N : -1;
        } else {
            return -1;
        }
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("Cant build pyramid");
        }
        Map<Integer, Integer> checkMap = new HashMap<>();
        for (Integer Number: inputNumbers) {
            if (checkMap.containsKey(Number)) {
                throw new CannotBuildPyramidException("Cant build pyramid");
            } else {
                checkMap.put(Number, 1);
            }
        }

        Collections.sort(inputNumbers);
        int N = findN(inputNumbers.size());
        if (N == -1) {
            throw new CannotBuildPyramidException("Cant build pyramid");
        } else {
            int M = findM(N);
            int k = 0;
            int[][] answer = new int[N][M];
            for (int index = 0; index < N; index++) {
                for (int j = 0; j < M; j++) {
                    answer[index][j] = 0;
                }

                int slice = N - index - 1;
                for (int j = 0; j <= index; j++) {
                    answer[index][slice] = inputNumbers.get(k++);
                    slice += 2;
                }
            }
            return answer;
        }
    }
}
