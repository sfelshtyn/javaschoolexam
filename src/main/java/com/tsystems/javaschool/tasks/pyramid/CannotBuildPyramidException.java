package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException(String cant_build_pyramid) {
        super(cant_build_pyramid);
    }
}
