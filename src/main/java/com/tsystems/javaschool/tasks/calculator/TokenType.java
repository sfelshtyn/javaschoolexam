package com.tsystems.javaschool.tasks.calculator;

public enum TokenType{
    SPACE,
    OPERAND,
    OPERATOR,
    DELIMITER
}