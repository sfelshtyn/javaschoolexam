package com.tsystems.javaschool.tasks.calculator;

public class CalculatorParseException extends Exception {
    public CalculatorParseException(String str) {
        super(str);
    }
}