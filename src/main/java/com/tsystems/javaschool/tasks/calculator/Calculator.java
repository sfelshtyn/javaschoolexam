package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.stream.IntStream;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static final String operators = "+-*/";
    private static final String delimiters = "() ";
    public static boolean flag = true;
    private static final Map<String, Integer> priorityMap = new HashMap<>();
    private static final Set<String> delimiterSet = new HashSet<>();
    private static final Set<String> operatorSet = new HashSet<>();

    static {
        priorityMap.put("(", 1);
        priorityMap.put("-", 2);
        priorityMap.put("+", 2);
        priorityMap.put("*", 3);
        priorityMap.put("/", 3);
        IntStream.range(0, delimiters.length()).mapToObj(i -> "" + delimiters.charAt(i)).forEach(delimiterSet::add);
        IntStream.range(0, operators.length()).mapToObj(i -> "" + operators.charAt(i)).forEach(operatorSet::add);
    }

    private static boolean isDelimiter(String token) {
        return delimiterSet.contains(token);
    }

    private static boolean isOperator(String token) {
        return operatorSet.contains(token);
    }

    private static int priority(String token) throws CalculatorParseException {
        if (token == null) {
            throw new CalculatorParseException("");
        }
        return priorityMap.get(token);
    }

    private static TokenType typeRecognizer(String token) {
        if (token.equals(" "))
            return TokenType.SPACE;
        if (isOperator(token))
            return TokenType.OPERATOR;
        if (isDelimiter(token))
            return TokenType.DELIMITER;
        return TokenType.OPERAND;
    }

    public static List<String> parse(String infix) throws CalculatorParseException {
        List<String> stringOut = new ArrayList<String>();
        Deque<String> stack = new ArrayDeque<String>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters + operators, true);
        String previous = "";
        String currentStr = "";

        while (tokenizer.hasMoreTokens()) {
            currentStr = tokenizer.nextToken();
            switch (typeRecognizer(currentStr)) {
                case SPACE:
                    continue;

                case OPERAND:
                    stringOut.add(currentStr);
                    break;

                case DELIMITER:
                    if (currentStr.equals("(")) stack.push(currentStr);
                    else if (currentStr.equals(")")) {
                        while (!stack.peek().equals("(")) {
                            stringOut.add(stack.pop());
                            if (stack.isEmpty()) {
                                throw new CalculatorParseException("");
                            }
                        }
                        stack.pop();
                    }
                    break;

                case OPERATOR:
                    if (!tokenizer.hasMoreTokens()) {
                        throw new CalculatorParseException("");
                    }
                    if (currentStr.equals("-") && (previous.equals("") || (isDelimiter(previous)  && !previous.equals(")")))) {
                        stringOut.add("0");
                    }
                    else {
                        while (!stack.isEmpty() && (priority(currentStr) <= priority(stack.peek()))) {
                            stringOut.add(stack.pop());
                        }

                    }
                    stack.push(currentStr);
                    break;
            }
            if (isOperator(previous) && isOperator(currentStr)) {
                throw new CalculatorParseException("");   
            }
            previous = currentStr;
        }

        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) {
                stringOut.add(stack.pop());
            }
            else {
                throw new CalculatorParseException("");
            }
        }
        return stringOut;
    }

    public static Double calc(List<String> strOut) throws CalculateException {
        Deque<Double> stack = new ArrayDeque<>();
        double x, y;
        for (String current : strOut) {
            if ("*".equals(current)) {
                stack.push(stack.pop() * stack.pop());
            } else if ("/".equals(current)) {
                y = stack.pop();
                if (y == 0)
                    throw new CalculateException("");
                x = stack.pop();
                stack.push(x / y);
            } else if ("+".equals(current)) {
                stack.push(stack.pop() + stack.pop());
            } else if ("-".equals(current)) {
                y = stack.pop();
                x = stack.pop();
                stack.push(x - y);
            } else {
                try {
                    stack.push(Double.parseDouble(current));
                    continue;
                } catch (NumberFormatException e) {
                    throw new CalculateException("");
                }
            }
        }
        return stack.pop();
    }

    public String evaluate(String statement) {
        if (statement == null || statement.equals(""))
            return null;
        String result;
        try {
            Double calculation = calc(parse(statement));
            if (calculation == Math.floor(calculation)) {
                result = String.valueOf(calculation.intValue());
            } else {
                result = String.valueOf(calculation);
            }
        } catch (CalculatorParseException | CalculateException cpe) {
            System.out.println(cpe.getMessage());
            return null;
        }
        return result;
    }

}